package ch.shaunbur.instantapp.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import ch.shaunbur.instantapp.R
import ch.shaunbur.instantapp.databinding.MainFragmentBinding
import com.google.android.instantapps.InstantApps

private const val REQUEST_CODE_INSTALL = 1337

class MainFragment : Fragment(), View.OnClickListener {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(MainFragmentBinding.inflate(inflater)) {
        lifecycleOwner = this@MainFragment
        vm = viewModel
        onClick = this@MainFragment
        root
    }

    override fun onClick(v: View) {
        when(v.id) {
            R.id.button_install_app -> showInstallPrompt()
        }
    }

    private fun showInstallPrompt() {
        val postInstall = Intent(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_DEFAULT)
            .setPackage("ch.shaunbur.instantapp")

        InstantApps.showInstallPrompt(
            requireActivity(),
            postInstall,
            REQUEST_CODE_INSTALL,
            null
        )
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}