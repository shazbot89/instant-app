package ch.shaunbur.instantapp.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class MainViewModel(app: Application) : AndroidViewModel(app) {
    val isInstantApp = MutableLiveData(app.packageManager.isInstantApp)
}