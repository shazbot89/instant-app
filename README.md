![Instant App cover image](cover.png "a title")
## A sample Instant App
- [x] Instant App Enabled
- [ ] Instant QR Code Scanner
- [ ] Show results in full app

<img src="instant-app.png" width="200"/>
